#  File: Nim.py

#  Description: Provides the first move to a game of Nim when given the number of elements in each pile

#  Student's Name: Christopher Calizzi

#  Student's UT EID:csc3322

#  Course Name: CS 313E

#  Unique Number: 50295

#  Date Created: 1-31-20

#  Date Last Modified: 1-31-20

#  Input: heaps is a list of integers giving the count in each heap
#  Output: function returns a single integer which is the nim-sum
def nim_sum(heaps):
    nim_sum = 0
    for num in heaps:
        nim_sum = num^nim_sum
    return nim_sum  # placeholder for the actual return statement


#  Input: heaps is a list of integers giving the count in each heap
#         nim_sum is an integer
#  Output: function returns two integers. The first integer is number
#          of counters that has to be removed. The second integer is
#          the number of the heap from which the counters are removed.
#          If the nim_sum is 0, then return 0, 0
def find_heap(heaps, nim_sum):
    #stores index of heaps being analyzed
    index = 0
    #stores stack being analyzed
    pile = 0
    #individual nim sum
    sum = 1
    while sum>=pile:
        pile = heaps[index]
        sum = pile^nim_sum
        index += 1
    return pile-sum, index


def main():
  # read input from input file nim.txt
  file = open("nim.txt")
  datasets = int(file.readline())
  for x in range(datasets):
    line = file.readline()
    heaps = []
    # reads heaps from lines of text
    while line:
        while " " in line and line.index(" ") == 0:
            line = line[1:len(line)]
        if " " in line:
            end = line.index(" ")
            heaps.append(int(line[0:end]))
            line = line[end:len(line)]
        else:
            heaps.append(int(line))
            line = ""
    # call function nim_sum() with inputs as given
    sum = nim_sum(heaps)
    if not sum:
        print("Lose Game")
    # call function find_heap() with inputs as given
    else:
        results = find_heap(heaps,sum)
    # print the results
        print("Remove " + str(results[0]) + " counters from Heap " + str(results[1]))
if __name__ == "__main__":
    main()